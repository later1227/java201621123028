package week5;

import java.util.ArrayList;
import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
		ArrayList<Fruit> fruitlist = new ArrayList<Fruit>();
		Scanner sc = new Scanner(System.in);
		int n;
		String str;
		
		n=sc.nextInt();
		Fruit[] fruitlist1=new Fruit[n];
		
		for(int i=0;i<n;i++){
			str=sc.next();
			fruitlist1[i]=new Fruit(str);
		}
		for(int i=0;i<fruitlist1.length;i++){
			if(!fruitlist.contains(fruitlist1[i]))
				fruitlist.add(fruitlist1[i]);
		}
		 for (Fruit fruit : fruitlist){
			System.out.println(fruit.getName());
		}
    }
		
	}
class Fruit{
	private String name;
	public Fruit(){
	
	}
	public Fruit(String name) {
		super();
		this.name = name;
	}
	 public String getName() {
	        return name;
	    }
	    public void haveName(String name) {
	        this.name = name;
	    }
	

	@Override
	public String toString() {
		return super.toString()+"-" + name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruit other = (Fruit) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}	
	
}