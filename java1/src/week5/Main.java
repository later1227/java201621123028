package week5;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int m=0;
		int n=0;
		String name;
        int age;
        boolean gender;
		int n1=sc.nextInt();
		PersonOverride[] persons1=new PersonOverride[n1];
		for(int i = 0;i<n1;i++){
			persons1 [i]=new PersonOverride();
		}
		int n2 =sc.nextInt();
		PersonOverride[] persons2=new PersonOverride[n2];
		for(int i = 0;i<n2;i++){
			n=0;
			if(m==0){
				
				persons2 [m]=new PersonOverride(sc.next(),sc.nextInt(), sc.nextBoolean());
				m++;
			}
			else{
				name=sc.next();
                age=sc.nextInt();
                gender=sc.nextBoolean();
				for(int j = 0;j<m;j++){
					if (persons2[j].equals(name,age,gender))
						n=-1;
				}
				if(n==0){
					persons2 [m]=new PersonOverride(name,age,gender);
					m++;
				}
			}
			
		}
		for(int i = 0;i<n1;i++){
			System.out.println(persons1[i]);
		}
		for(int i = 0;i<m;i++){
			System.out.println(persons2[i]);
		}
		System.out.println(m);
		System.out.println(Arrays.toString(PersonOverride.class.getConstructors()));
	}

}
class PersonOverride{
	private String name;
	private boolean gender;
	private int age;
	
	public PersonOverride(){
		name="default";
		age=1;
		gender=true;
	}
	public PersonOverride(String name,int age,boolean gender){
		this.name=name;
		this.age=age;
		this.gender=gender;
	}
	@Override
	public String toString() {
		return name+"-"+age+"-"+gender;
	}
	


	public boolean equals(String name,int age,boolean gender) {
		//return (this.name==name)&&(age==this.age)&&(gender==this.gender);
	       return name.equals(this.name)&&(age==this.age)&&(gender==this.gender);
	    }
	
		
	
}