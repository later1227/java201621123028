package week7;

import java.util.ArrayList;



import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author henry
 *ռ��201621123928
 */


	@SuppressWarnings("serial")
	public class ShoppingCart implements Serializable{
	private ArrayList<Commodity> Commodities=new ArrayList<>();
	public ArrayList<Commodity> getCommodities() {
			return Commodities;
		}

	public void setCommodities(ArrayList<Commodity> commodities) {
			Commodities = commodities;
		}

	@Override
	public String toString() {
			return "ShoppingCart [Commodities=" + Commodities + "]";
		}

	public boolean isEmpty() {
			return Commodities.isEmpty();
		}
		
		
	public boolean remove(Object o) {
			return Commodities.remove(o);
		}
		
		

	public Commodity remove(int index) {
			if(index>=Commodities.size())
			return Commodities.remove(index);
			return null;
		}

	public boolean add(Commodity e) {
			if (!Commodities.contains(e)) {
				return Commodities.add(e);
			}
		else {
				int n=Commodities.indexOf(e);
				Commodities.get(n).setNumber(e.getNumber()+Commodities.get(n).getNumber());
				return true;
			}
		}

	public boolean reduceNumber(int n,int m) {
			if(Commodities.get(n).getNumber()<m)
				return false;
			else 
			{
				Commodities.get(n).setNumber(Commodities.get(n).getNumber()-m);
				return true;
			}
		}
		
	public double Sum(){
			double sum=0;
			for(Commodity e:Commodities)
				sum=sum+e.Sum();
			return sum;
		}

	public ShoppingCart(ArrayList<Commodity> commodities) {
			super();
			Commodities = commodities;
	}

	public ShoppingCart() {
			super();
			// TODO Auto-generated constructor stub
	}

	public void clear() {
			Commodities.clear();
	}
	public double Checkout(){
			double sum=Sum();
			clear();
			return sum;
	}
		
}
//	private Commodity[] Commodities=new Commodity[100];
//	private int N;
//	public Commodity[] getCommodities() {
//		return Commodities;
//	}
//	
//	public void setCommodities(Commodity[] commodities) {
//		Commodities = commodities;
//	}
//
//	public boolean isEmpty() {
//		return N==0;
//	}
//	public boolean add(Commodity e) {
//		for (int i = 0; i < N; i++) {
//			if(Commodities[i].equals(e))
//			{
//				return false;
//			}
//		}
//		Commodities[N]=e;
//		N++;
//		return false;
//		
//	}

