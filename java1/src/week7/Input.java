package week7;

import java.util.Scanner;

public class Input {
	private static Scanner in=new Scanner(System.in);
	public static int Int()
	{
		return in.nextInt();
	}
	public static Double Double()
	{
		return in.nextDouble();
	}
	
	public static String NextLine() {
		return in.nextLine();
	}
	public static String Next() {
		return in.next();
	}
	
	
}