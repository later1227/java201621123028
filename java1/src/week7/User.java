package week7;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class User implements Serializable,DAO{
	private String Name;
	private String address;
	private String Account;
	private String Password;
	private double Money;
	private ShoppingCart Cart;
	private int flag;
	
	
	
	
	public User() {
		Name=null;
		address=null;
		Account = null;
		Password = null;
		Cart = null;
		Money=0;
		flag=0;
	}
	public User(String name, String address, String account, String password,double money,int flag) {
		super();
		Name = name;
		this.address = address;
		Account = account;
		Password = password;
		Cart = new ShoppingCart();
		Money=money;
		this.flag=flag;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Account == null) ? 0 : Account.hashCode());
		result = prime * result + ((Password == null) ? 0 : Password.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (Account == null) {
			if (other.Account != null)
				return false;
		} else if (!Account.equals(other.Account))
			return false;
		if (Password == null) {
			if (other.Password != null)
				return false;
		} else if (!Password.equals(other.Password))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		return "User [Name=" + Name + ", address=" + address + ", Account=" + Account + ", Money=" + Money  + ", flag=" + flag + "]";
	}
	public void clear(){
		Cart.clear();
	}
	
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public ShoppingCart getCart() {
		return Cart;
	}

	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getAccount() {
		return Account;
	}
	public double getMoney() {
		return Money;
	}
	public void setMoney(double money) {
		Money = money;
	}
	public double Checkout(){
		Money=Money-Cart.Checkout();
		return Money;
	}
	public void writeObjectToFile(Object obj) {
		File file =new File("//home//zyb//EclipseWorkspace//ShoppingMall//Userinfo.txt");
        FileOutputStream out;
        try {
            out = new FileOutputStream(file);
            ObjectOutputStream objOut=new ObjectOutputStream(out);
            @SuppressWarnings("unchecked")
			ArrayList<User> user=(ArrayList<User>) obj;
            if(user.indexOf(this)>=0)
            user.add(this);
            else
            	user.set(user.indexOf(this),this);
			objOut.writeObject(obj);
            objOut.flush();
            objOut.close();
            System.out.println("write object success!");
        } catch (IOException e) {
            System.out.println("write object failed");
            e.printStackTrace();
        }
		
	}

	@Override
	public Object readObjectToFile() {
		Object temp=null;
		Object temp1=null;
        File file =new File("//home//zyb//EclipseWorkspace//ShoppingMall//Userinfo.txt");
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            ObjectInputStream objIn=new ObjectInputStream(in);
            temp=objIn.readObject();
            @SuppressWarnings("unchecked")
			ArrayList<User> user=(ArrayList<User>) temp;
           for (User user2 : user) {
			if((user2.getAccount().compareTo(this.getAccount()))==0&&(user2.getPassword().compareTo(this.getPassword()))==0 ){
            	this.Cart=user2.getCart();
            	this.address=user2.getAddress();
            	this.flag=user2.getFlag();
            	this.Money=user2.getMoney();
            	this.Name=user2.getName();
			}	
		}
            objIn.close();
        } catch (IOException e) {
            System.out.println("read object failed");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return temp1;
	}
	@Override
	public void diplayAll(Object user) {
		System.out.println(user.toString());
	}
}



