package week7;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Commodity implements Serializable,DAO{
	private String GoodsName;
	private double Price;
	private int Number;
	private int BianHao;
	

	public int getBianHao() {
		return BianHao;
	}
	public void setBianHao(int bianHao) {
		BianHao = bianHao;
	}
	public String getGoodsName() {
		return GoodsName;
	}
	public void setGoodsName(String goodsName) {
		GoodsName = goodsName;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public int getNumber() {
		return Number;
	}
	
	public void setNumber(int number) {
		Number = number;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((GoodsName == null) ? 0 : GoodsName.hashCode());
		result = prime * result + Number;
		long temp;
		temp = Double.doubleToLongBits(Price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commodity other = (Commodity) obj;
		if (GoodsName == null) {
			if (other.GoodsName != null)
				return false;
		} else if (!GoodsName.equals(other.GoodsName))
			return false;
		if (Double.doubleToLongBits(Price) != Double
				.doubleToLongBits(other.Price))
			return false;
		return true;
	}
	public double Sum(){
		return Number*Price;
	}
	public Commodity(String goodsName, double price, int number,int bianhao) {
		super();
		GoodsName = goodsName;
		Price = price;
		Number = number;
		BianHao=bianhao;
	}
	public Commodity() {
		super();
		
	}
	
	
	@Override
	public String toString() {
		return "Commodity [GoodsName=" + GoodsName + ", Price=" + Price + ", Number=" + Number + ", BianHao=" + BianHao
				+ "]";
	}
	@Override
	public void writeObjectToFile(Object obj) {
		File file =new File("//home//zyb//EclipseWorkspace//ShoppingMall//CommodityInformation.txt");
        FileOutputStream out;
        try {
            out = new FileOutputStream(file);
            ObjectOutputStream objOut=new ObjectOutputStream(out);
            @SuppressWarnings("unchecked")
			ArrayList<Commodity> commodity=(ArrayList<Commodity>) obj;
            if(!commodity.contains(this))
            commodity.add(this);
			objOut.writeObject(obj);
            objOut.flush();
            objOut.close();
            System.out.println("write object success!");
        } catch (IOException e) {
            System.out.println("write object failed");
            e.printStackTrace();
        }
		
	}

	@Override
	public Object readObjectToFile() {
		Object temp=null;
        File file =new File("//home//zyb//EclipseWorkspace//ShoppingMall//CommodityInformation.txt");
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            ObjectInputStream objIn=new ObjectInputStream(in);
            temp=objIn.readObject(); 
            objIn.close(); 
            System.out.println("read object success!");
        } catch (IOException e) {
            System.out.println("read object failed");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return temp;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void diplayAll(Object user) {
		
		ArrayList<Commodity> users;
		if(user!=null)
		{
			users=(ArrayList<Commodity>) user;
			for (Commodity user2 : users) {
				System.out.println(user2);
			}
		}
	}
}


