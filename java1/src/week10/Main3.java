package week10;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

enum Gender{man,woman}
public class Main3 {

	public static void main(String[] args) {
		List<Student> stuList=new ArrayList<Student>();
		
		stuList.add(new Student((long)10,"zhansan",20,Gender.man,true));
		stuList.add(new Student((long)11,"lisi",19,Gender.woman,false));
		stuList.add(null);
		stuList.add(new Student((long)13,"henry",20,Gender.man,false));
		stuList.add(new Student((long)23,"wangqiang",22,Gender.man,true));
		stuList.add(new Student((long)18,"zhan",20,Gender.man,true));
		stuList.add(new Student((long)15,"xiaoming",23,Gender.man,true));
		stuList.add(new Student((long)21,"xiaohong",17,Gender.woman,true));
		stuList.add(null);
		List<Student> Student=search(stuList, 11L, "zhan", 20, Gender.man, true);
		for(int i=0;i<Student.size();i++){
			System.out.println("占恒201621123028");
			System.out.println(Student.get(i));
			
		}
			
		
	}
	static List<Student> search(List<Student> stuList, Long id, String name, int age, Gender gender, boolean joinsACM){
		
		List<Student> stuList1=stuList.stream().filter(Student->Student!=null&&Student.getId()>id&&Student.getName().compareTo(name)==0&&Student.getAge()==age&&Student.getGender()==gender&&Student.isJoinsACM()==joinsACM).collect(Collectors.toList());
		return stuList1;
			
	}
//	static List<Student> search(List<Student> stuList, Long id, String name, int age, Gender gender, boolean joinsACM){
//	List<Student> stuList1=new ArrayList<Student>();
//	for(int i =0;i<stuList.size();i++)
//	{
//		if(stuList.get(i).getId()>id&&stuList.get(i).getName().compareTo(name)==0&&stuList.get(i).getAge()==age&&stuList.get(i).getGender()==gender&&stuList.get(i).isJoinsACM()==joinsACM)
//			stuList1.add(stuList.get(i));
//	}
//	return stuList1;	
//}

}
class Student{
	private Long id;
	private String name;
	private int age;
	private Gender gender;//枚举类型
	private boolean joinsACM; //是否参加过ACM比赛
	public List<Student> stuList=new ArrayList<Student>();
	public Student(Long id, String name, int age, Gender gender, boolean joinsACM) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.joinsACM = joinsACM;
		
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", joinsACM=" + joinsACM
				+ "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public boolean isJoinsACM() {
		return joinsACM;
	}

	public void setJoinsACM(boolean joinsACM) {
		this.joinsACM = joinsACM;
	}

	
	
}
