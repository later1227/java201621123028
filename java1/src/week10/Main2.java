package week10;

import java.util.ArrayList;
import java.util.Scanner;



public class Main2 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int m,n;
		while(true){
			String str=sc.next();
			int flag=0;
			switch(str){
			case"Integer":
				Integer sum=0;
				m=sc.nextInt();
				n=sc.nextInt();
				ArrayListGeneralStack stack=new ArrayListGeneralStack(m); 
				System.out.println("Integer Test");
				for(int i=0;i<m;i++){
					System.out.println("push:"+stack.push(sc.nextInt()));
				}
				for(int i=0;i<n;i++){
					System.out.println("pop:"+stack.pop());
				}
				for(int i=0;i<stack.size();i++){
					Integer j=(Integer)stack.getList().get(i);
					sum+=j;
				}
					System.out.println(stack.toString());
					System.out.println("sum="+sum);
				System.out.println(stack.getClass().getInterfaces()[0]);
                break;
			case"Double":
				Double sum2=0.0;
				m=sc.nextInt();
				n=sc.nextInt();
				ArrayListGeneralStack stack1=new ArrayListGeneralStack(m); 
				System.out.println("Double Test");
				for (int i = 0; i < m; i++) {
                    System.out.println("push:" + stack1.push(sc.nextDouble()));
                }
                for (int i = 0; i < n; i++) {
                    System.out.println("pop:" + stack1.pop());
                }
               
                for (int i = 0; i < stack1.size(); i++) {
                    Double j = (Double) stack1.getList().get(i);
                    sum2 += j;
                }
            	   System.out.println(stack1);
           		System.out.println("sum="+sum2);
                System.out.println(stack1.getClass().getInterfaces()[0]);
                break;
			case"Car":
				m=sc.nextInt();
				n=sc.nextInt();
				ArrayListGeneralStack stack2=new ArrayListGeneralStack(m); 
				System.out.println("Car Test");
				for (int i = 0; i < m; i++) {
					System.out.println("push:" + stack2.push(new Car(sc.nextInt(), sc.next())));
                }
                for (int i = 0; i < n; i++) {
                    System.out.println("pop:" + stack2.pop());
                }
               if(m>=n){
            	   System.out.println(stack2);
              		for (int i = stack2.size()-1; i >=0; i--) {
                  		System.out.println(((Car)stack2.getList().get(i)).getName());
             		 } 
               }
               else 
               {
           		 System.out.println(stack2);
           		 System.out.println("null");
               }
           System.out.println(stack2.getClass().getInterfaces()[0]);
           break;
			case"quit":
				flag=1;
				break;
			default:
				break;
			}
			if(flag==1)
				break;

		}

	}
}
class ArrayListGeneralStack implements GeneralStack{

	private ArrayList<Object> List = new ArrayList<>();
    private int n;
    
	public ArrayListGeneralStack(int n) {
		List = new ArrayList<>();
        this.n = n;
	}


	public ArrayList<Object> getList() {
		return List;
	}

	public void setList(ArrayList<Object> list) {
		List = list;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return List.toString();
	}

	@Override
	public Object push(Object item) {
		if(item == null)
			return null;
		List.add(item);
		return item;
	}

	@Override
	public Object pop() {
		if(List.size()==0)
			return null;
		Object a;
		a= List.get(List.size()-1);
		List.remove(List.size()-1);
		return a;
	}

	@Override
	public Object peek() {
		if(List.size()==0)
			return null;
		return List.get(List.size()-1);
	}

	@Override
	public boolean empty() {
		return List.size() == 0;
	}

	@Override
	public int size() {
		return List.size();
	}
	
}

class Car{
	private int id;
	private String name;
	public Car(int id, String name) {
        this.id = id;
        this.name = name;
    }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	
}
