package week10;

public interface GeneralStack<T> {

	T push(T item);            //如item为null，则不入栈直接返回null。
	T pop();                 //出栈，如为栈为空，则返回null。
	T peek();                //获得栈顶元素，如为空，则返回null.
	public boolean empty();//如为空返回true
	public int size();     //返回栈中元素数量
}
