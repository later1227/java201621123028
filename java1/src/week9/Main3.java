package week9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class Main3 {

	public static void main(String[] args) {
	Map<String, Integer> words=new TreeMap<String, Integer>();
		Scanner sc = new Scanner(System.in);
		while(true){
			String word = sc.next();
			if(word.equals("!!!!!"))
				break;
			else if(words.containsKey(word))
			{
				words.put(word, words.get(word)+1);
			}
			else
				words.put(word, 1); 
		}
			System.out.println(words.size());
			
		List<Map.Entry<String, Integer>> list = new ArrayList<>(words.entrySet());  
		  Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){
				@Override
				public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
					if(o1.getValue().compareTo(o2.getValue())==0)
						return o1.getKey().compareTo(o2.getKey());
					else
					return o2.getValue().compareTo(o1.getValue());
				}
	        });
		if(words.size()>10){
			
			for(int i=0;i<10;i++)
			{
				System.out.println(list.get(i).getKey()+"="+list.get(i).getValue());
			}	
		}
		else
			for(int i=0;i<words.size();i++)
			System.out.println(list.get(i).getKey()+"="+list.get(i).getValue());
			
		}
		
}


