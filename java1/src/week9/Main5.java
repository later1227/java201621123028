package week9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main5 {

     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            List<String> list = convertStringToList(sc.nextLine());
            System.out.println(list);
            String word = sc.nextLine();
            remove(list,word);
            System.out.println(list);
        }
        sc.close();
       
}
     public static List<String> convertStringToList(String line){
         List<String> strlist=new ArrayList<String>();
         Scanner sc=new Scanner(line);
         while(sc.hasNext())
         {
             strlist.add(sc.next());
         }
         sc.close();
         return strlist;
     }
     
     public static void remove(List<String> list, String str){
         
         for (int i = 0; i < list.size(); i++) {
             if(list.get(i).equals(str)){
                 list.remove(i);
                 i--;
             }
         }
     }
}