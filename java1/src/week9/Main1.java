package week9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Main1 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n;
		Integer m;
		n=sc.nextInt();
		ArrayIntegerStack arrayIntegerStack=new ArrayIntegerStack(n);
		for(int i=0;i<n;i++){
			m=sc.nextInt();
			System.out.println(arrayIntegerStack.push(m));
		}
		System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
		System.out.println(arrayIntegerStack.getArrList());
		n=sc.nextInt();
		for(int i =0;i<n;i++){
			System.out.println(arrayIntegerStack.pop());
		}
		System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
		System.out.println(arrayIntegerStack.getArrList());
		
		
		
	}
	
}


class ArrayIntegerStack implements IntegerStack{
private List<Integer> arrList = new ArrayList<Integer>();
private int n;

public ArrayIntegerStack(int n) {
	super();
	this.n = n;
}


public List<Integer> getArrList() {
	return arrList;
}



@Override
public Integer push(Integer item) {
	if(item == null)
		return null;
	arrList.add(item);
	return item;
}

@Override
public Integer pop() {
	if(arrList.size()==0)
	return null;
	Integer a;
	a=arrList.get(arrList.size()-1);
	arrList.remove(arrList.size()-1);
	return a;
}

@Override
public Integer peek() {
	if(arrList.size()==0)
		return null;
	return arrList.get(arrList.size()-1);
}

@Override
public boolean empty() {
	return arrList.size()==0?true:false;
}

@Override
public int size() {
	
	return arrList.size();
}

@Override
public String toString() {
	return arrList.toString();
}


}
