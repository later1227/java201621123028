package week8;


import week8.ShoppingCart;

/**
 *
 * @author henry
 */
public class User {
    private String Name;	//用户名
    private String address;	//地址
    private String Account;//账号
    private String Password;//密码
    private double Money;	//充值的金额
    private ShoppingCart Cart;	//存储购物车信息
    private int flag;		

    public void setAccount(String Account) {
        this.Account = Account;
    }

    public void setCart(ShoppingCart Cart) {
        this.Cart = Cart;
    }

  //无参构造函数
    public User() {
        Name = null;
        address = null;
        Account = null;
        Password = null;
        Cart = null;
        Money = 0;
        flag = 0;
    }
    //有参构造函数，对类进行初始化，。
    public User(String name, String address, String account, String password, double money, int flag) {
        super();
        Name = name;
        this.address = address;
        Account = account;
        Password = password;
        Cart = new ShoppingCart();
        Money = money;
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Account == null) ? 0 : Account.hashCode());
        result = prime * result + ((Password == null) ? 0 : Password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (Account == null) {
                return false;
            
        } else if (Account.compareTo(other.Account)!=0) {
            return false;
        }
        if (Password == null) {
            if (other.Password != null) {
                return false;
            }
        } else if (Password.compareTo(other.Password)!=0) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User [Name=" + Name + ", address=" + address + ", Account=" + Account + ", Money=" + Money + ", flag=" + flag + "]";
    }

    public void clear() {
        Cart.clear();
    }
  //下面的setter()和getter()方法，是提供一个对类中的私有属性设置以及访问的方法。
    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public ShoppingCart getCart() {
        return Cart;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAccount() {
        return Account;
    }

    public double getMoney() {
        return Money;
    }

    public void setMoney(double money) {
        Money = money;
    }
    //实现结账
    public double Checkout() {
        Money = Money - Cart.Checkout();
        return Money;
    }
    
   

    public void diplayAll(Object user) {
        System.out.println(user.toString());
    }
}
