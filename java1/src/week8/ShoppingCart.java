package week8;


import java.util.ArrayList;

import week8.Commodity;

public class ShoppingCart {
	private ArrayList<Commodity> Commodities=new ArrayList<>();//有序存放商品
	public ArrayList<Commodity> getCommodities() {
		return Commodities;
	}

	public void setCommodities(ArrayList<Commodity> commodities) {
		Commodities = commodities;
	}

	@Override
	public String toString() {
		return "ShoppingCart [Commodities=" + Commodities + "]";
	}
	//判断购物车是否为空
	public boolean isEmpty() {
		return Commodities.isEmpty();
	}
	
	//判断要删除的对象是否在列表中
	public boolean remove(Object o) {
		return Commodities.remove(o);
	}
	
	
	//移除此列表中指定位置上的商品，移动所有后续商品，还要将索引减 1。
	public Commodity remove(int index) {
		if(index>=Commodities.size())
		return Commodities.remove(index);
		return null;
	}
	//添加商品到购物车，如果购物车没有该商品，直接添加进去，如果有的话，只需要添加对应的数量就好了。
	public boolean add(Commodity e) {
		if (!Commodities.contains(e)) {
			return Commodities.add(e);
		}
		else {
			int n=Commodities.indexOf(e);
			Commodities.get(n).setNumber(e.getNumber()+Commodities.get(n).getNumber());
			return true;
		}
	}

//	public boolean reduceNumber(int n,int m) {
//		if(Commodities.get(n).getNumber()<m)
//			return false;
//		else 
//		{
//			Commodities.get(n).setNumber(Commodities.get(n).getNumber()-m);
//			return true;
//		}
//	}
	//对加入购物车的所有商品进行求和
	public double Sum(){
		double sum=0;
		for(Commodity e:Commodities)
			sum=sum+e.Sum();
		return sum;
	}

	public ShoppingCart(ArrayList<Commodity> commodities) {
		super();
		Commodities = commodities;
	}

	public ShoppingCart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void clear() {
		Commodities.clear();
	}
	//结账时应该花费多少钱，在用户类里被调用
	public double Checkout(){
		double sum=Sum();
		clear();
		return sum;
	}
	
}
