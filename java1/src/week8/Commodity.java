package week8;


public class Commodity  {

    private int Identifier;//编号
    private String GoodsName;//商品
	private String Brand;//品牌
    private double Price;//价格
    private int Number;//商品的数量
    
    //下面的setter()和getter()方法，是提供一个对类中的私有属性设置以及访问的方法。
	public String getGoodsName() {
		return GoodsName;
	}
	public void setGoodsName(String goodsName) {
		GoodsName = goodsName;
	}
	public String getBrand() {
		return Brand;
	}
	public void setBrand(String brand) {
		Brand = brand;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public int getNumber() {
		return Number;
	}
	public void setNumber(int number) {
		Number = number;
	}
	public int getIdentifier() {
		return Identifier;
	}
	public void setIdentifier(int identifier) {
		Identifier = identifier;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Brand == null) ? 0 : Brand.hashCode());
		result = prime * result + ((GoodsName == null) ? 0 : GoodsName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		// 如果另一个对象和当前对象引用都一样，那么肯定是同一个对象实例，返回true
		if (this == obj)
			return true;
		//如果另一个对象为null，返回false
		if (obj == null)
			return false;
		//如果另一个对象和当前对象类型都不一样，那么肯定不相等，返回false
		if (getClass() != obj.getClass())
			return false;
		//这时候传进来的对象和当前对象类型一样了，那就转换下
		Commodity other = (Commodity) obj;
		if (Brand == null) {
			if (other.Brand != null)					//如果Brand为null而传进来的other.Brand却不为null，肯定不相等，返回false
				return false;
		} else if (!Brand.equals(other.Brand))			//other.Brand不为空，直接调用equals方法判断两个Brand是不是一样
			return false;
		if (GoodsName == null) {
			if (other.GoodsName != null)				//如果GoodsName为null而传进来的other.GoodsName却不为null，肯定不相等，返回false
				return false;
		} else if (!GoodsName.equals(other.GoodsName))	//other.GoodsName不为空，直接调用equals方法判断两个Brand是不是一样
			return false;
		return true;									//相等的话返回true
	}
	
	//无参构造函数
	public Commodity() {
		super();
		
	}
	
	//有参构造函数，对类进行初始化，。
	public Commodity(int identifier, String goodsName, String brand, double price, int number) {
		super();//super调用构造方法
		//将形参传给实参
		Identifier = identifier;
		GoodsName = goodsName;
		Brand = brand;
		Price = price;
		Number = number;
	}
	
	//对某件商品求和，比如某商品有多件时。
	public double Sum() {
		
		return Number * Price;
	}
	@Override
	public String toString() {
		return "Commodity [Identifier=" + Identifier + ", GoodsName=" + GoodsName + ", Brand=" + Brand + ", Price="
				+ Price + ", Number=" + Number + "]";
	}
	


}
