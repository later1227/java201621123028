package week8;

import week8.Commodity;
import java.util.ArrayList;
import week8.ShoppingCart;
import week8.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class WalMart {
	
	//用户注册，注册之后写入User的数据库中
	public static User registere(User user) {
        user.setFlag(1);
        user.setCart(new ShoppingCart());
        writeUserTosql(user);
        return user;
    }
	//用户登录，调用，看里面是否有输进去的信息
    public static User signin(User user) {
        if (user.getFlag() == 0) {
        	user=readUserTosql(user);
            return user;
        } else {
            return user;
        }

    }
    
    public static void writeUserTosql(User user) {		//将用户信息写进数据库
    	Connection con = null;
        PreparedStatement pStatement = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名
        try {
            
            Class.forName(driverName);
            con = DriverManager.getConnection(url, userName, password);			//登录的实现动作，返回连接到的对象，以进行操作。
            String strSql = "insert into Userinfo(account,password,name,address,flag) values(?,?,?,?,?)";		//对Userinfo表的插入格式，参数化的sql查询语句。
            pStatement = con.prepareStatement(strSql);								//要执行strSql所对应的sql语句
            //下面是设置每个参数的值，从列表1开始。
            pStatement.setString(1,user.getAccount());//账号
            pStatement.setString(2, user.getPassword());//密码
            pStatement.setString(3, user.getName());//名字
            pStatement.setString(4,user.getAddress());//地址
            pStatement.setInt(5, user.getFlag());
            pStatement.executeUpdate();						//执行sql语句
            pStatement.close();
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static User readUserTosql(User user) {			//查询数据库中的用户信息
        Connection con = null;
        PreparedStatement pStatement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名

        try {
            
            Class.forName(driverName); 
            con = DriverManager.getConnection(url, userName, password);	//登录的实现动作，返回连接到的对象，以进行操作。

            String strSql = "select * from Userinfo";					//参数化的sql查询语句，查询Userinfo表中的信息。
            pStatement = con.prepareStatement(strSql);					//要执行strSql所对应的sql语句
            rs = pStatement.executeQuery(strSql);						//开始执行sql语句
            //下面的循环是判断账号和密码对应用户，对应正确的用户查询的购物车信息才是自己的
            while (rs.next()) {
                String Account = rs.getString("account");
                String Password = rs.getString("password");
                if(user.getAccount().equals(Account)&&user.getPassword().equals(Password))	//判断
                {
                	 user.setFlag(1);
                	 user=readshoppingcartsql(user);	//可以查询自己的购物车
                	 break;
                }

            }
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (rs != null)
                try {
                    rs.close();
                    rs = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }
    public static void writeCommodityInformationTosql(Commodity commodity) {			//添加商品到购物商城的界面上
    	Connection con = null;
        PreparedStatement pStatement = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名
        try {			//捕获异常
            
            Class.forName(driverName);  	//加载驱动
            con = DriverManager.getConnection(url, userName, password);
            String strSql = "insert into CommodityInformation(goodname,brand,price) values(?,?,?)";	//对CommodityInformation表的插入格式，参数化的sql查询语句。
            pStatement = con.prepareStatement(strSql);												//要执行strSql所对应的sql语句
          //下面是设置每个参数的值，从列表1开始，3结束。
            pStatement.setString(1,commodity.getGoodsName());
            pStatement.setString(3, commodity.getBrand());
            pStatement.setDouble(2, commodity.getPrice());
            pStatement.executeUpdate();						//执行sql语句
            pStatement.close();								//关闭连接表的对象
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static ArrayList<Commodity> readCommodityInformationTosql() {			//查询购物商城界面上的商品信息
        ArrayList<Commodity> temp = new ArrayList<>();								//声明temp为ArrayList类型的一个对象
        Connection con = null;
        PreparedStatement pStatement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名

        try {
            
            Class.forName(driverName);  
            con = DriverManager.getConnection(url, userName, password);

            String strSql = "select * from CommodityInformation";			//参数化的sql查询语句，查询CommodityInformation表中的信息。 
            pStatement = con.prepareStatement(strSql);					//要执行strSql所对应的sql语句
            rs = pStatement.executeQuery(strSql);						//开始执行sql语句
            
          //从数据库中读取对应信息
            int i=0;
            while (rs.next()) {
            	Commodity a=new Commodity();
            	a.setGoodsName(rs.getString("goodname"));
            	a.setBrand(rs.getString("brand"));
            	a.setPrice(rs.getDouble("price"));
            	a.setIdentifier(i+1);
            	i++;
            	temp.add(a);
            }
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (rs != null)
                try {
                    rs.close();
                    rs = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return temp;
    }
    
    
    public static void writeshoppingcartsql(User user) {		//将购物车的信息写进数据库中保存
        PreparedStatement ps = null;
        Connection con = null;
        PreparedStatement pStatement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名

        try {
            
            Class.forName(driverName);  //运行驱动
            con = DriverManager.getConnection(url, userName, password);//连接数据库的表
            String strSql = "select * from ShoppingCar";				//参数化的sql查询语句，查询ShoppingCar表中的数据。	
            pStatement = con.prepareStatement(strSql);
            rs = pStatement.executeQuery(strSql);						//开始执行sql语句
            
            //将放入购物车的信息写进数据库中
            while (rs.next()) {
                String goodname = rs.getString("goodname");
                String account = rs.getString("account");
                //先确定登录进去的账号和数据库里面的账号对应
                if(user.getAccount().equals(account))
                {		
                	//进行遍历
                	 for(int i=0;i<user.getCart().getCommodities().size();i++)
                	 {
                		 if(user.getCart().getCommodities().get(i).getGoodsName().equals(goodname))
                		 {
                			 //用于更新表中已存在的记录。
                			 strSql = "update ShoppingCar set number=? where account=? and goodname=?";
                			 ps = con.prepareStatement(strSql);				//要执行strSql所对应的sql语句
                			//下面是设置每个参数的值，从列表1开始，3结束。
                			 ps.setInt(1,user.getCart().getCommodities().get(i).getNumber());//商品数量
                			 ps.setString(2, user.getAccount());							//账号
                			 ps.setString(3,user.getCart().getCommodities().get(i).getGoodsName());//商品数量
                			 user.getCart().getCommodities().get(i).setIdentifier(-1);		//商品的编号
                			 break;
                		 }
                	 }
                }

            }
            //实现插入新的信息
            for(int i=0;i<user.getCart().getCommodities().size();i++) {
            	if(user.getCart().getCommodities().get(i).getIdentifier()!=-1)//首先判断不为空
            	{
            		strSql="insert into ShoppingCar(account,goodname,brand,price,number) values(?,?,?,?,?)";//对ShoppingCar表的插入格式，参数化的sql查询语句。
                	ps = con.prepareStatement(strSql);//要执行strSql所对应的sql语句
                	//下面是设置每个参数的值，从列表1开始，5结束，分别对应账号、商品名、品牌、价格、数量。
                	ps.setString(1,user.getAccount());
                	ps.setString(2,user.getCart().getCommodities().get(i).getGoodsName());
                	ps.setString(3,user.getCart().getCommodities().get(i).getBrand());
                	ps.setDouble(4,user.getCart().getCommodities().get(i).getPrice());
                	ps.setInt(5,user.getCart().getCommodities().get(i).getNumber());
                	ps.executeUpdate();			//执行strSql所对应的sql语句
                	ps.close();
            	}
            	
            }
            
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (rs != null)
                try {
                    rs.close();
                    rs = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (ps != null)
                try {
                	ps.close();
                	ps = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            }
         
        }
    }
    public static User readshoppingcartsql(User user) {
        Connection con = null;
        PreparedStatement pStatement = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://localhost:3306/Shopping";		//通过jdbc驱动与mysql连接数据库中名为Shopping的数据库
        String userName = "root";									//登录数据库的用户名和密码
        String password = "later1227";
        String driverName = "com.mysql.jdbc.Driver";				//驱动程序名

        try {
            
            Class.forName(driverName);  
            con = DriverManager.getConnection(url, userName, password);		//连接数据库的表
            String strSql = "select * from ShoppingCar";	//参数化的sql查询语句，查询ShoppingCar表中的数据。	
            pStatement = con.prepareStatement(strSql);		
            rs = pStatement.executeQuery(strSql);			//开始执行sql语句
            ShoppingCart s=new ShoppingCart();
            ArrayList<Commodity> commodities = new ArrayList<>();
          //从数据库中读取ShoppingCart对应信息
            while (rs.next()) {
                String goodname = rs.getString("goodname");
                String account = rs.getString("account");
                String brand = rs.getString("brand");
                int number = rs.getInt("number");
                Double price = rs.getDouble("price");
                
                //匹配用户
                if(user.getAccount().equals(account))
                {
                	 commodities.add(new Commodity(0,goodname,brand,price,number));
                }

            }
            s.setCommodities(commodities);
            user.setCart(s);
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("没有找到驱动类!");
            e.printStackTrace();
        }
        finally {
            if (rs != null)
                try {
                    rs.close();
                    rs = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (pStatement != null)
                try {
                    pStatement.close();
                    pStatement = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }

}


