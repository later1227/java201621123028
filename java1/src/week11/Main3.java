package week11;

import java.util.Scanner;

public class Main3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[]a=new int[5]; 
		boolean f = true;
		String str;
		while(true){
			str=sc.next();
			switch(str){
			
				case"arr":
					try{
						int n=sc.nextInt();
						a[n]=n;
					}catch(ArrayIndexOutOfBoundsException e){
						System.out.println(e);
						break;
					}
					break;
				case"null":
					try{
						a=null;
					    a[0]=0;
					}catch(NullPointerException e){
						System.out.println(e);
						a=new int[5];
						break;
					}
					break;
				case"cast":
					try{
						Object x = new String("12");
						System.out.println((Integer)x);
					}catch(ClassCastException e){
						System.out.println(e);
						break;
					}
					break;
				case"num":
					try{
						String s=sc.next();
						Integer i=Integer.parseInt(s);
					}catch(NumberFormatException e){
						System.out.println(e);
						break;
					}
					break;
				default:
					f=false;
					break;
			}
			if(f=false)
				break;
			
		}
		

	}

}
