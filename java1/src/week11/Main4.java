package week11;

import java.util.Scanner;

public class Main4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int begin = 0,end = 0;
		int n = Integer.parseInt(sc.nextLine());
		double []arr=new double [n];
		for(int i=0;i<arr.length;i++)
		{
			arr[i] = sc.nextDouble();
		}
		while(true){
			try{
				begin=sc.nextInt();
				end=sc.nextInt();
			}catch (Exception e) {
				break;
			}
			try{
				System.out.println(ArrayUtils.findMax(arr, begin, end));
			}catch(IllegalArgumentException e){
				System.out.println(e);
			}
			
		}
		try {
			System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
			System.out.println(e1);
		}

	}

}

class ArrayUtils{
	
	
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException {
		
		if(begin>=end)
		throw new IllegalArgumentException("begin:"+begin +" >"+"= "+"end:"+end);
		else if(begin<0)
			throw new IllegalArgumentException("begin:"+begin +" < "+"0");
		else if(end>arr.length)
			throw new IllegalArgumentException("end:"+end+" > "+"arr.length");
		Double max=0.0;
		for(int i=begin;i<end;i++){
			if(arr[i]>max)
				max=arr[i];
		}
		return max;
	}
}