package week11;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class Mian7 {

	public static void main(String[] args) {
		byte[] content = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("D:\\testfis.txt");
		} catch (FileNotFoundException e2) {
			// TODO 自动生成的 catch 块
			System.out.println("找不到文件testfis.txt，请重新输入文件名");
			e2.printStackTrace();
			
		}
		int bytesAvailabe = 0;
		try {
			bytesAvailabe = fis.available();
		} catch (IOException e1) {
			// TODO 自动生成的 catch 块
			System.out.println("打开或读取文件失败!");
			e1.printStackTrace();
		}//获得该文件可用的字节数
		if(bytesAvailabe>0){
		    content = new byte[bytesAvailabe];//创建可容纳文件大小的数组
		    try {
				fis.read(content);
			} catch (IOException e) {
				
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}//将文件内容读入数组
		    finally{
		    	try{
		    		System.out.println("关闭文件ing");
		    		fis.close();
		    	}catch(Exception e2){
		    		System.out.println("关闭文件失败!");
		    	}
		    }
		}
		System.out.println(Arrays.toString(content));//打印数组内容

	}

}
