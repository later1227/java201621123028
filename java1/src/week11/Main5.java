package week11;

import java.util.EmptyStackException;

public class Main5 {

	public static void main(String[] args) {
		

	}

}
class ArrayIntegerStack implements IntegerStack{
    private int capacity;
    private int top=0;
    private Integer[] arrStack;
    public Integer push(Integer item) throws FullStackException {
		if(item==null) {
			return null;
		}
	if(top==capacity){
	  throw new FullStackException();
	}
	arrStack[top]=item;
		top+=1;
		return item;
		
	}                               
	public Integer pop() throws EmptyStackException {
	 if(top==0){
	    throw new EmptyStackException();
	    
	  }else{
	  int a= arrStack[top-1];
	  top=top-1;
	  return a;}
	  	
	}    
	public Integer peek() throws EmptyStackException {
	  if(top==0){
	    throw new EmptyStackException();
	    
	  }else
	  return arrStack[top-1];	
	}
	
}


