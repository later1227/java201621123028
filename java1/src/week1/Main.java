package week1;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) 
	{	
		
		Scanner sc = new Scanner(System.in);
		while(sc.hasNextLine())
		{
			String i=sc.nextLine();
			int sum = 0;
			char[] c=i.toCharArray();
			int len = c.length;
			for(int b=0;b<len;b++)
			{
				if(c[b]=='-'||c[b]=='.')
					continue;
				sum+=Integer.parseInt(i.substring(b,b+1));
			}
			System.out.println(sum);
		}
		
		sc.close();
	}

}
