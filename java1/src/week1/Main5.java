package week1;

import java.util.Scanner;

public class Main5 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while(in.hasNextLine()){
		
			double x=in.nextDouble();
			if(x<0)
				System.out.println("NaN");
			else{
				double a=0;
				while(!(x-a*a<0.0001||a*a>=x)){
					a=a+0.0001;	
				}
				System.out.printf("%.6f\n",a);
			
			}
		}
		in.close();
	}

}