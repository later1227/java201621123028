package week3;

import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
		while(true){
			int n;
			Scanner sc = new Scanner(System.in);
			n=Integer.parseInt(sc.nextLine());
			
			Person persons[] = new Person[n];
			for(int i = 0;i<persons.length;i++){
				Person person=new Person(sc.next(),sc.nextInt(),sc.nextBoolean());
				persons[i]=person;
			}
			for (int j= persons.length-1;j>=0;j--){
				System.out.println(persons[j]);
				
			}
			System.out.println(new Person());
			
		}
		}
	}

 class Person {
	private String name;
	private int age;
	private boolean gender;
	private int id;
	
	public Person(){
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d\n",name,age,gender,id);
		
	}
	public Person(String name,int age, boolean gender){
		this.name=name;
		this.gender=gender;
		this.age=age;
	}
	
	public void setname(String name) {
		this.name=name;
		
	}
	public String getname(){
		return name;
	}
	
	public void setgender(boolean gender) {
		this.gender=gender;
		
	}
	public boolean isgender(){
		return gender;
	}
	
	
	public void setage(int age) {
		this.age=age;
		
	}
	public int setage(){
		return age;
	}
	
	

	public String toString(){
		return String.format("Person [name=%s, age=%d, gender=%s, id=%d]",name,age,gender,id);
	}
 }