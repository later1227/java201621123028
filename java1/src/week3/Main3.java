package week3;

import java.util.Arrays;
import java.util.Scanner;

public class Main3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(true){
		Rectangle[] rec=new Rectangle[2];
		Circle[] cir=new Circle[2];
		rec[0]=new Rectangle(sc.nextInt(),sc.nextInt());
		rec[1]=new Rectangle(sc.nextInt(),sc.nextInt());
		cir[0]=new Circle(sc.nextInt());
		cir[1]=new Circle(sc.nextInt());
		int a=rec[0].getPerimeter()+rec[1].getPerimeter()+cir[0].getPerimeter()+cir[1].getPerimeter();
		int b=rec[0].getArea()+rec[1].getArea()+cir[0].getArea()+cir[1].getArea();
		System.out.println(a);
		System.out.println(b);
		System.out.println(Arrays.deepToString(rec));
		System.out.println(Arrays.deepToString(cir));
		}	

	}

}
class Rectangle{
	 private int  width;
	 private int  length;

 public Rectangle(int width,int length){
	 this.width=width;
	 this.length=length;
 }
 
 
 
 @Override
public String toString() {
	return "Rectangle [width=" + width + ", length=" + length + "]";
}



public int getWidth()
 {
     return width;
 }
 public void setWidth(int width)
 {
     this.width = width;
 }
 public int getLength()
 {
     return length;
 }
 public void setLength(int length)
 {
     this.length = length;
 }
 public int getPerimeter(){
	 return (width+length)*2;
 }
 public int getArea()

 {
     return width*length;
 }
}

 class Circle{
	private int radius;
 
	
	public Circle(int radius){	
		this.radius=radius;
	}
	public int getr(){
		return radius;
	}
	public int getPerimeter(){
		return (int) (2*Math.PI*radius);
	}
	
	public int getArea(){
		return (int) (Math.PI*radius*radius);
	}
	
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
}

