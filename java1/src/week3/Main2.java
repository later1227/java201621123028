package week3;

import java.util.Scanner;

public class Main2 {

	public static void main(String[] args) {
		while(true){

			int n;
			Scanner sc = new Scanner(System.in);
			n=Integer.parseInt(sc.nextLine());
			
			Person persons[] = new Person[n];
			for(int i = 0;i<persons.length;i++){
				Person person=new Person(sc.next(),sc.nextInt(),sc.nextBoolean());
				persons[i]=person;
			}
			for (int j= persons.length-1;j>=0;j--){
				persons[j].setid(j);
				System.out.println(persons[j]);
				
			}
			persons[persons.length-1].setid(persons.length);
			System.out.println(new Person());
			
		}
		}
	}

 class Person{
	private String name;
	private int age;
	private boolean gender;
	static private int id;
	static{
		System.out.println("This is static initialization block");
	}
	{
		System.out.printf("This is initialization block, id is %d\n",(id++));
	}
	
	public Person(){
		
		System.out.println("This is constructor");
		System.out.printf("%s,%d,%s,%d\n",name,age,gender,(--id));
		
	}


	public Person(String name,int age, boolean gender){
		this.name=name;
		this.gender=gender;
		this.age=age;
		
	}
	public String getname(){
		return name;
	}
	public void setname(String name) {
		this.name=name;
		
	}
	public boolean isgender(){
		return gender;
	}
	public void setgender(boolean gender) {
		this.gender=gender;
		
	}
	public int setage(){
		return age;
	}
	public void setage(int age) {
		this.age=age;
		
	}
	public int getid(){
		return id;
	}
	public void setid(int id){
		this.id=id;
	}
	public String toString(){
		return String.format("Person [name=%s, age=%d, gender=%s, id=%d]",name,age,gender,id);
	}
	
 }
 
