package week2;
	
	
import java.util.Arrays;
import java.util.Scanner;

public class Main6 {
	
	private static Scanner sc;

	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		while(sc.hasNextInt()){
			
			int n = sc.nextInt();
			String array[][]=new String[n][0];
			for(int i=0;i<n;i++)
			{	
				array[i]=new String[i+1];
				for(int j =0;j<array[i].length;j++)
				{
					array[i][j]=(i+1)+"*"+(j+1)+"="+(j+1)*(i+1);
				}
				
			}
			for(int i=0;i<array.length;i++)
			{
				for(int j=0;j<array[i].length;j++)
				{
					if(i==j)
						System.out.println(array[i][j]);
					else
						System.out.printf("%-7s",array[i][j]);//%-7s,题目要求7个字符，加-就是左对齐。
				}
			}

			System.out.println(Arrays.deepToString(array));
		}
	}	

}
