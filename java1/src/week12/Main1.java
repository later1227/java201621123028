package week12;

import java.util.Scanner;

class MyThread extends Thread{
	private int n;
	public MyThread(int parseInt) {
		this.n=parseInt;
	}

	@Override
	public void run() {
		for (int i = 0; i < n; i++) {
			System.out.println(i);
			
		}
		System.out.println(Thread.currentThread().getName()+" "+isAlive());
	}
	
}

public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Thread t1 = new MyThread(Integer.parseInt(sc.next()));
        t1.start();
    }
}
