package week12;

import java.util.Scanner;

class Account{
	private int balance;
	
	
	public Account(int balance) {
		this.balance = balance;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
	/*
	 * 201621123028
	 */
	public void deposit(int money){
		synchronized(this)
		{
			balance=money+balance;
		}
		
	}
	public void withdraw(int money){
		synchronized(this)
		{
			balance=balance-money;
		}
		
	}

}


public class Main6 {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根

	}

}
