package week12;

import java.util.Scanner;

class PrintTask implements Runnable{
	private int n;
	public PrintTask(int parseInt) {
		this.n=parseInt;
	}

	@Override
	public void run() {
		for (int i = 0; i < n; i++) {
			System.out.println(i);
			
		}
		System.out.println(Thread.currentThread().getName());
	}
	
}

public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PrintTask task = new PrintTask(Integer.parseInt(sc.next()));
        Thread t1 = new Thread(task);
        t1.start();
        sc.close();
    }
}
