package week6;

import java.util.Arrays;

public class Test {

	public static void main(String[] args) {
		Person[] persons=new Person[3];
		persons[0]=new Person("zhang",15);
		persons[1]=new Person("zhang",12);
		persons[2]=new Person("wang",14);
		Arrays.sort(persons);
		System.out.println(Arrays.toString(persons));
	}

}
class Person implements Comparable<Person>{
	private String name;
	private int age;
	public Person(String name,int age){
		this.name=name;
		this.age=age;
		
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

	@Override
	public int compareTo(Person o) {
		if(name==o.name)
			return name.compareTo(o.name);
		return
			o.age-age;
	}
	
	
}
