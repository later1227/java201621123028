package week6;


import java.util.Arrays;
import java.util.Scanner;



public class Main2 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n;
		Integer k;
		n=sc.nextInt();
		ArrayIntegerStack arrayIntegerStack=new ArrayIntegerStack(n);
		n=sc.nextInt();
		for(int i=0;i<n;i++){
			k=sc.nextInt();
			System.out.println(arrayIntegerStack.push(k));
		}
		System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
		System.out.println(Arrays.toString(arrayIntegerStack.getInts()));
		n=sc.nextInt();
		for(int i =0;i<n;i++){
			System.out.println(arrayIntegerStack.pop());
		}
		System.out.println(arrayIntegerStack.peek()+","+arrayIntegerStack.empty()+","+arrayIntegerStack.size());
		System.out.println(Arrays.toString(arrayIntegerStack.getInts()));
		
	}

}
interface IntegerStack {
	public Integer push(Integer item);
	public Integer pop();
	public Integer peek();
	public boolean empty();
	public int size();
}
class ArrayIntegerStack implements IntegerStack{
	private Integer[] ints;
	private int top=0;
	private int n;
	
	public Integer[] getInts() {
		return ints;
	}

	public ArrayIntegerStack(int n) {
		ints=new Integer[n];
		this.n=n;
	}

	@Override
	public Integer push(Integer item) {
		if(item==null)
		return null;
		if(top>n-1)
			return null;
		ints[top++]=item;
		return item;
	}

	@Override
	public Integer pop() {
		if(top==0)
			return null;
		Integer m = ints[top-1];
		top--;
		return m;
	}

	@Override
	public Integer peek() {
		if(top==0)
			return null;
		return ints[top-1];
	}

	@Override
	public boolean empty() {
		return top==0;
	}

	@Override
	public int size() {
		
		return top;
	}
	
}

