package week6;

import java.util.Arrays;
import java.util.Scanner;

public class Main1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n,m;
		String s;
		n=sc.nextInt();
		PersonSortable[] persons=new PersonSortable[n];
		for(int i=0;i<n;i++){
			s=sc.next();
			m=sc.nextInt();
			persons[i]=new PersonSortable(s,m);
		}
		Arrays.sort(persons);
		for(int i=0;i<persons.length;i++){
			
			System.out.println(persons[i]);
		}
		System.out.println(Arrays.toString(PersonSortable.class.getInterfaces()));

	}

}
class PersonSortable implements Comparable<PersonSortable>{

	private String name;
	private int age;
	
	
	public PersonSortable(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}


	@Override
	public String toString() {
		return name+"-"+age;
	}

	@Override
	public int compareTo(PersonSortable o) {
			return toString().compareTo(o.toString());
	
	}
	
	
	
}