package week6;




import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;




import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;



public class Main{

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n=0;
		int m=0;
		String s=null;
		n=sc.nextInt();
		PersonSortable2[] persons=new PersonSortable2[n];
		for(int i=0;i<n;i++){
			s=sc.next();
			m=sc.nextInt();
			persons[i]=new PersonSortable2(s,m);
		}
		Arrays.sort(persons,new NameComparator());
		System.out.println("NameComparator:sort");
		for(int i=0;i<persons.length;i++){
			
			System.out.println(persons[i]);
		}
		Arrays.sort(persons,new AgeComparator());
		System.out.println("AgeComparator:sort");
		for(int i=0;i<persons.length;i++){
			
			System.out.println(persons[i]);
		}
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}
	



}
class PersonSortable2 {

	private String name;
	private int age;
	
	
	public PersonSortable2(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}


	@Override
	public String toString() {
		return name+"-"+age;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}



}

class NameComparator implements Comparator<PersonSortable2>{

@Override
public int compare(PersonSortable2 o1, PersonSortable2 o2) {
	
	return o1.getName().compareTo(o2.getName());
}

}

class AgeComparator implements Comparator<PersonSortable2>{

	@Override
	public int compare(PersonSortable2 o1, PersonSortable2 o2) {
		
		return o1.getAge()-o2.getAge();
	}
	
}
