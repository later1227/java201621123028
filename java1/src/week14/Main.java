package week14;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<Commodity> commodities = new ArrayList<>();
		ArrayList<User> users = new ArrayList<>();
		commodities.add(new Commodity("Nausicaa of the Valley of the Winds",20 ,0 ,1));
		commodities.add(new Commodity("One Piece", 24, 0, 2));
		commodities.add(new Commodity("Meishidefulu", 33, 0, 3));
		commodities.add(new Commodity("Bookshelves", 200, 0, 4));
		commodities.add(new Commodity("Computer", 3000, 0, 5));
		users.add(new User("zyb","xiamen","1000000","1234567890",0,1));
		users.add(new User("Lu","xiamen","1000001","1234567890",0,1));
		users.add(new User("SH","xiamen","1000002","1234567890",0,1));
		users.add(new User("EN","xiamen","1000003","1234567890",0,1));
		users.add(new User("ND","xiamen","1000004","1234567890",0,1));
		WalMart.writeUserToFile(users);
		WalMart.writeCommodityInformationToFile(commodities);

	}

}
