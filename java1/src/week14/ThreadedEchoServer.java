package week14;

import java.io.*;
import java.net.*;
import java.util.*;

public class ThreadedEchoServer
{  
   public static void main(String[] args )
   {  
      try
      {  
         int i = 1;
         ServerSocket s = new ServerSocket(8189);

         while (true)
         {  
            Socket incoming = s.accept();
            System.out.println("Spawning " + i);
            Runnable r = new ThreadedEchoHandler(incoming);
            Thread t = new Thread(r);
            t.start();
            i++;
         }
      }
      catch (IOException e)
      {  
         e.printStackTrace();
      }
   }
}


class ThreadedEchoHandler implements Runnable
{
   public ThreadedEchoHandler(Socket i)
   { 
      incoming = i; 
   }

   public void run()
   {  
      try
      {  
         try
         {
            InputStream inStream = incoming.getInputStream();
            OutputStream outStream = incoming.getOutputStream();
            String s="";
            Scanner in = new Scanner(inStream);         
            PrintWriter out = new PrintWriter(outStream, true /* autoFlush */);
            User user=new User();
            
            
            // echo client input
            boolean done = false;
            while (!done && in.hasNextLine())
            {  
               out.println( "1.登陆" );
               out.println( "2.注册" );
               out.println( "3.商城" );
               out.println( "4.购物车" );
               out.println( "5.退出" );
               String line = in.nextLine();

               switch(line)
               {
                  case "1":
                  if (user.getFlag()==0) {
                     out.println( "请输入账号和密码:" );
                     String account=in.nextLine();
                     String password=in.nextLine();
                     user.setAccount(account);
                     user.setPassword(password);
                     user=WalMart.signin(user);
                  }
                  else {
                     out.println("登陆成功!");
                  }
                  break;
                     

                  case "2":
                     out.println( "请输入名字 , 邮箱地址 , 密码:" );
                     user.setName(in.nextLine());
                     user.setAddress(in.nextLine());
                     user.setPassword(in.nextLine());
                     user=WalMart.registere(user);
                     break;
                  case "3":
                     if (user.getFlag()!=0) {
                     ArrayList<Commodity> commodities = WalMart.readCommodityInformationToFile();
                     for (int i=0;i<commodities.size()  ;i++ ) {

                        byte[] bs = commodities.get(i).getGoodsName().getBytes();
                        String sk=new String(bs, "GBK");
                        String str = "[商品名=" + sk + ", 价格=" + commodities.get(i).getPrice() + ", 数量=" + commodities.get(i).getNumber() + ", 编号=" + commodities.get(i).getBianHao()
                + "]";
                        System.out.println(str);
                        
                        out.println(str);
                     }
                     String k = in.nextLine();
                     out.println("1.购买");
                     out.println("2.返回");
                     if(k.equals("1"))
                     {
                        out.println("输入想购买的商品编号:");
                        String g=in.nextLine();
                        int a = Integer.parseInt(g);
                        user.getCart().add(new Commodity(commodities.get(a-1).getGoodsName(),commodities.get(a-1).getPrice(),1,commodities.get(a-1).getBianHao()));
                        ArrayList<User> users = WalMart.readUserToFile();
                        int b = users.indexOf(user);
                        users.get(b).setCart(user.getCart());
                        WalMart.writeUserToFile(users); 
                     }
                     }
                     else {
                         out.println("请先登录");
                     }
                     
                     break;
                  case "4":
                     if (user.getFlag()!=0) {
                        for (int i=0;i<user.getCart().getCommodities().size()  ;i++ ) {
                        out.println("商品名 = "+user.getCart().getCommodities().get(i).getGoodsName()+" \n价格 = "+user.getCart().getCommodities().get(i).getPrice()+"\n数量 = "+user.getCart().getCommodities().get(i).getNumber());
                        out.println("总和 = "+user.getCart().Sum());
                     }
                     }
                     else {
                        out.println("请先登录");
                     }
                     break;
                  default:
                     break;
               }
               out.println("Echo: " + line);
               if (line.trim().equals("5"))
                  done = true;
            }
         }
         finally
         {
            incoming.close();
         }
      }
      catch (IOException e)
      {  
         e.printStackTrace();
      }
   }

   private Socket incoming;
}

