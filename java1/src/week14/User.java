package week14;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zyb
 */
@SuppressWarnings("serial")
public class User implements Serializable {
    private static final long serialVersionUID = 3L;
    private String Name;
    private String address;
    private String Account;
    private String Password;
    private double Money;
    private ShoppingCart Cart;
    private int flag;

    public void setAccount(String Account) {
        this.Account = Account;
    }

    public void setCart(ShoppingCart Cart) {
        this.Cart = Cart;
    }

    
    public User() {
        Name = null;
        address = null;
        Account = null;
        Password = null;
        Cart = null;
        Money = 0;
        flag = 0;
    }

    public User(String name, String address, String account, String password, double money, int flag) {
        super();
        Name = name;
        this.address = address;
        Account = account;
        Password = password;
        Cart = new ShoppingCart();
        Money = money;
        this.flag = flag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Account == null) ? 0 : Account.hashCode());
        result = prime * result + ((Password == null) ? 0 : Password.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (Account == null) {
                return false;
            
        } else if (Account.compareTo(other.Account)!=0) {
            return false;
        }
        if (Password == null) {
            if (other.Password != null) {
                return false;
            }
        } else if (Password.compareTo(other.Password)!=0) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User [Name=" + Name + ", address=" + address + ", Account=" + Account + ", Money=" + Money + ", flag=" + flag + "]";
    }

    public void clear() {
        Cart.clear();
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public ShoppingCart getCart() {
        return Cart;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAccount() {
        return Account;
    }

    public double getMoney() {
        return Money;
    }

    public void setMoney(double money) {
        Money = money;
    }

    public double Checkout() {
        Money = Money - Cart.Checkout();
        return Money;
    }
    
    public void readToFile() {
        ArrayList<User> temp ;
        File file = new File("D:/java/���繺�ﳵ/ShoppingMall/Userinfo.txt");
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            ObjectInputStream objIn;
            try {
                objIn = new ObjectInputStream(in);
                try {
                    temp = (ArrayList<User>) objIn.readObject();
                    objIn.close();
                    if(temp.contains(this))
                    {
                        int i=temp.indexOf(this);
                        this.Cart=temp.get(i).getCart();
                        this.address=temp.get(i).getAddress();
                        this.flag=temp.get(i).getFlag();
                        this.Money=temp.get(i).getMoney();
                        this.Name=temp.get(i).getName();
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void diplayAll(Object user) {
        System.out.println(user.toString());
    }
}
