package week14;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Commodity implements Serializable {

    private static final long serialVersionUID = 2L;
    private String GoodsName;
    private double Price;
    private int Number;
    private int BianHao;

    public int getBianHao() {
        return BianHao;
    }

    public void setBianHao(int bianHao) {
        BianHao = bianHao;
    }

    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((GoodsName == null) ? 0 : GoodsName.hashCode());
        result = prime * result + Number;
        long temp;
        temp = Double.doubleToLongBits(Price);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Commodity other = (Commodity) obj;
        if (GoodsName == null) {
            return false;
        } else if (GoodsName.compareTo(other.GoodsName)!=0) {
            return false;
        }
        return Double.doubleToLongBits(Price) == Double
                .doubleToLongBits(other.Price);
    }

    public double Sum() {
        return Number * Price;
    }

    public Commodity(String goodsName, double price, int number, int bianhao) {
        super();
        GoodsName = goodsName;
        Price = price;
        Number = number;
        BianHao = bianhao;
    }

    public Commodity() {
        super();
    }

    @Override
    public String toString() {
        return "Commodity [GoodsName=" + GoodsName + ", Price=" + Price + ", Number=" + Number + ", BianHao=" + BianHao
                + "]";
    }

}
