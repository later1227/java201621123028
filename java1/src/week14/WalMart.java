package week14;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WalMart {

    public static User registere(User user) {
        ArrayList<User> users = readUserToFile();
        user.setAccount("" + (users.size() + 1000000)); 
        user.setFlag(1);
        user.setCart(new ShoppingCart());
        users.add(user);
        
        writeUserToFile(users);
        return user;
    }

    public static User signin(User user) {
        if (user.getFlag() == 0) {
            user.readToFile();
            return user;
        } else {
            return user;
        }

    }

    public static void writeUserToFile(Object obj) {
        File file = new File("D:/java/���繺�ﳵ/ShoppingMall/Userinfo.txt");
        FileOutputStream out;
        try {
            out = new FileOutputStream(file);
            try (ObjectOutputStream objOut = new ObjectOutputStream(out)) {
                objOut.writeObject(obj);
                objOut.flush();
                objOut.close();
            } catch (IOException ex) {
                Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ArrayList<User> readUserToFile() {
        ArrayList<User> temp = null;
        File file = new File("D:/java/���繺�ﳵ/ShoppingMall/Userinfo.txt");
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            ObjectInputStream objIn;
            try {
                objIn = new ObjectInputStream(in);
                try {
                    temp = (ArrayList<User>) objIn.readObject();
                    objIn.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            } catch (IOException ex) {
                Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return temp;
    }

    public static void writeCommodityInformationToFile(Object obj) {
        File file = new File("D:/java/���繺�ﳵ/ShoppingMall/CommodityInformation.txt");
        FileOutputStream out;
        try {
            out = new FileOutputStream(file);
            try (ObjectOutputStream objOut = new ObjectOutputStream(out)) {
                objOut.writeObject(obj);
                objOut.flush();
                objOut.close();
            } catch (IOException ex) {
                Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static ArrayList<Commodity> readCommodityInformationToFile() {
        ArrayList<Commodity> temp = null;
        File file = new File("D:/java/���繺�ﳵ/ShoppingMall/CommodityInformation.txt");
        FileInputStream in;
        try {
            in = new FileInputStream(file);
            ObjectInputStream objIn;
            try {
                objIn = new ObjectInputStream(in);
                try {
                    temp = (ArrayList<Commodity>) objIn.readObject();
                    objIn.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
                }
               
            } catch (IOException ex) {
                Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WalMart.class.getName()).log(Level.SEVERE, null, ex);
        }
        return temp;
    }

}
